# GdUnit generated TestSuite
class_name ExampleTest
extends GdUnitTestSuite
@warning_ignore('unused_parameter')
@warning_ignore('return_value_discarded')


func test_do_thing() -> void:
	# Just return true. This is an example for CICD.
	assert_bool(true).is_true()
